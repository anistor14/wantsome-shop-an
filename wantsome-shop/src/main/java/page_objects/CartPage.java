package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {
    private final WebDriver driver;
    private static final By CART_TITLE = By.className("entry-title");
    private static final By CART_MESSAGE = By.className("cart-empty");
    public final By CART_VALUE = By.className("cart-value");

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getCartTitle(){
        return driver.findElement(CART_TITLE).getText();
    }

    public String getCartMessage(){
        return driver.findElement(CART_MESSAGE).getText();
    }

    public int getCartValue(){
        return Integer.parseInt(driver.findElement(CART_VALUE).getText());
    }

}
