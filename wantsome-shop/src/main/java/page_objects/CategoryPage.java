package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CategoryPage {
    WebDriver driver;
    private static final By CATEGORY_TITLE = By.className("entry-title");
    public final By CART_ICON = By.className("add_to_cart_button");  // By.cssSelector("[id='primary'] [class*='fa-shopping-cart']");
    public final By PRODUCT_IMAGE = By.className("products-img");

    public CategoryPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getCategoryTitle(){
        return driver.findElement(CATEGORY_TITLE).getText();
    }

    public void addToCart() {
        Actions hoverImage = new Actions(driver);
        hoverImage.moveToElement(driver.findElement(PRODUCT_IMAGE)).build().perform();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable((CART_ICON)));
        driver.findElement(CART_ICON).click();
    }

}
