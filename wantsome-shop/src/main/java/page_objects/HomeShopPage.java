package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class HomeShopPage {
    private final WebDriver driver;
    private static final By CART_BUTTON = By.xpath("//a[text()='Cart']");
    private final By CATEGORY_BUTTON = By.className("category-toggle");
    private final By MEN_COLLECTION = By.id("menu-item-509");
    private final By SEARCH_ICON = By.className("search-icon");
    private final By SEARCH_FIELD = By.className("search-field");


    public HomeShopPage(WebDriver driver) {
        this.driver = driver;
    }

    public CartPage clickOnCart() {
        driver.findElement(CART_BUTTON).click();
        return new CartPage(driver);
    }

    public void clickOnCategory(){
        driver.findElement(CATEGORY_BUTTON).click();
    }

    public CategoryPage clickOnManCollection(){
        driver.findElement(MEN_COLLECTION).click();
        return new CategoryPage(driver);
    }

    public void clickOnSearchIcon(){
        driver.findElement(SEARCH_ICON).click();
    }

    public SearchResultsPage typeSearch(String searchInput){
        driver.findElement(SEARCH_FIELD).sendKeys(searchInput);
        driver.findElement(SEARCH_FIELD).sendKeys(Keys.ENTER);
        return new SearchResultsPage(driver);
    }
}
