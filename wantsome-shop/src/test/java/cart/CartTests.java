package cart;

import org.junit.Test;
import org.openqa.selenium.By;
import page_objects.CartPage;
import page_objects.CategoryPage;
import page_objects.HomeShopPage;
import utils.BaseTestClass;

import static org.junit.Assert.*;

public class CartTests extends BaseTestClass {

    @Test
    public void checkEmptyCart() {
        //click on Cart tab
        driver.findElement(By.xpath("//a[text()='Cart']")).click();

        // check Cart page title
        assertEquals("CART", driver.findElement(By.className("entry-title")).getText());

        //check Cart page message
        assertEquals("Your cart is currently empty.", driver.findElement(By.className("cart-empty")).getText());
    }

    @Test
    public void checkEmptyCartPageObject() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        CartPage cartPage = homeShopPage.clickOnCart();

        // check Cart page title
        assertEquals("CART", cartPage.getCartTitle());

        //check Cart page message
        assertEquals("Your cart is currently empty.", cartPage.getCartMessage());
    }

    @Test
    public void checkCartValue() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage menCollection = homeShopPage.clickOnManCollection();
        menCollection.addToCart();
        CartPage cartPage = new CartPage(driver);
        assertEquals(1,cartPage.getCartValue());
    }
}
