package category;

import org.junit.Test;
import page_objects.CategoryPage;
import page_objects.HomeShopPage;
import utils.BaseTestClass;

import static org.junit.Assert.assertEquals;

public class CategoryTests extends BaseTestClass {

    @Test
    public void checkMenCollectionTitle(){
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage menCollection = homeShopPage.clickOnManCollection();
        assertEquals("Men Collection", menCollection.getCategoryTitle());
    }
}
