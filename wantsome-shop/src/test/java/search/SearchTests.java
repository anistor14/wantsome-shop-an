package search;

import org.junit.Test;
import page_objects.HomeShopPage;
import page_objects.SearchResultsPage;
import utils.BaseTestClass;

import static org.junit.Assert.*;

public class SearchTests extends BaseTestClass {

    @Test
    public void checkSearchResults(){
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnSearchIcon();
        SearchResultsPage searchResults = homeShopPage.typeSearch("watch");
        assertTrue(searchResults.checkResultsContainSearch("watch"));
    }
}
