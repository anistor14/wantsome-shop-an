package utils;

import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class BaseTestClassFirefox {
    private static final String URL = "https://testare-automata.practica.tech/blog/";
    public WebDriver driver;

    @Before
    public void setUpTest() {
//        System.setProperty("webdriver.chrome.driver",
//                DriversPath.getDriverDirPath() + "chromedriver" + DriversPath.getDriverExtension());
//        ChromeOptions options = new ChromeOptions();
//        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
//        driver = new ChromeDriver(options);

        driver=BrowserFactory.getBrowser("firefox");

        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get(URL);
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}